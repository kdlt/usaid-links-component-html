# USAID Universal LINKS Footer HTML

This repository is the static HTML version of the USAID universal 
LINKS footer Drupal module located at 
[https://gitlab.com/kdlt/usaid-links-component](https://gitlab.com/kdlt/usaid-links-component)

## Getting started
There are two HTML files in this repository, one for a dark theme 
and one for a light theme as shown below.

![Dark theme](admin/images/screencap-dark-theme.png "Dark Theme")
Above, the "dark" theme, standard and hover states.

![Light theme](admin/images/screencap-light-theme.png "Light Theme")
Above, the "light" theme, standard and hover states.

How you implement this in your website may vary
depending on your specific platform such as WordPress or another CMS. It may be
that you write a custom plugin or just use the static HTML in a block.

You can clone this repository using:

```bash
git clone git@gitlab.com:kdlt/usaid-links-component-html.git
```

... or [download a zip file archive](https://gitlab.com/kdlt/usaid-links-component-html/-/archive/1.0.0/usaid-links-component-html-1.0.0.zip).

Noting that it would be ideal for your site to include the html
code below in the `<head>` of your page:

```html
<meta name="viewport" content="width=device-width, initial-scale=1">
```

If you cannot include this code or your site does not have this tag,
you may experience issues with media queries working properly.

## Code and assets overview
All the necessary images, CSS, and JS are in the assets folder. There are two CSS files
that are potentially optional, `misc.css` and `normailize.css`. The main CSS file, 
`usaid-link-sites.css` is required and works for both the light and dark themes.
There are also a few comments in the HTML files that may come in handy.

## Getting help
If you need additional help, [please contact Rachel Plett](mailto:rachel.plett@bixal.com) at Bixal Solutions Inc.
